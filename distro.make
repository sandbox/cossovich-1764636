; Include Build Kit distro makefile via URL
includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/distro.make

; Add leaflet_kickstart to the full Drupal distro build
projects[leaflet_kickstart][type] = profile
projects[leaflet_kickstart][download][type] = git
projects[leaflet_kickstart][download][url] = http://git.drupal.org/sandbox/cossovich/1764636.git
