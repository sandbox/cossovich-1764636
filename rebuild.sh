#!/bin/bash
rm -rf modules/contrib
rm -rf themes/base
rm -rf libraries
drush make --no-gitinfofile --working-copy --no-core --contrib-destination=. leaflet_kickstart.make .
# Usually run update.php and clear the cache here
