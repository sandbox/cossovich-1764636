Leaflet Kickstart for Drupal 7.x
--------------------------------
Evaluate a GIS solution using Leaflet post-haste!

Leaflet Kickstart is a quick way for developers to evaluate a Drupal 7 based
GIS solution using the Leaflet module in combination with the Address Field,
Geocoder and Geofield projects. Also included in the distribution is the
Leaflet More Maps module which provides the Leaflet output with additional map
styles.


Getting started
---------------
Leaflet Kickstart is a quick way to evaluate Dr

1. Grab the `distro.make` file from Leaflet Kickstart and run:

    $ drush make distro.make [directory]

  or use its url on Drupal.org directly:

    $ drush make --prepare-install "http://drupalcode.org/sandbox/cossovich/1764636.git/blob_plain/refs/heads/master:/distro.make" [directory]

2. Choose the "Leaflet Kickstart" install profile when installing Drupal.


Using Drush for a quick install
-------------------------------
If you just want to quickly evaluate Leaflet Kickstart without configuring a
server, you could try the Drush to automatically install and run the
distribution using Drush's in-built server. After running the drush make step
above, change into the Drupal root directory and run the following:

    $ drush si --db-url=sqlite://sites/default/files/localdb.sqlite leaflet_kickstart --yes && drush rs /admin/reports/status --user=admin
