; Include Build Kit install profile makefile via URL
includes[] = http://drupalcode.org/project/buildkit.git/blob_plain/refs/heads/7.x-2.x:/drupal-org.make


; GIS contrib modules =========================================================

projects[addressfield][subdir] = contrib
projects[addressfield][version] = 1.0-beta3

projects[geocoder][subdir] = contrib
projects[geocoder][version] = 1.2

projects[geofield][subdir] = contrib
projects[geofield][version] = 1.1

projects[geophp][subdir] = contrib
projects[geophp][version] = 1.6

projects[leaflet][subdir] = contrib
projects[leaflet][version] = 1.0-beta1

projects[leaflet_more_maps][subdir] = contrib
projects[leaflet_more_maps][version] = 1.0-alpha2

projects[libraries][subdir] = contrib
projects[libraries][version] = 2.0


; Additional contrib modules ==================================================

projects[module_filter][subdir] = contrib
projects[module_filter][version] = 1.7

projects[defaultcontent][subdir] = contrib
projects[defaultcontent][version] = 1.0-alpha6

; Patch for Default Content regarding the following: http://drupal.org/node/1446714
projects[defaultcontent][patch][] = "http://drupal.org/files/defaultcontent-1446714-16.patch"

projects[uuid][subdir] = contrib
projects[uuid][version] = 1.0-alpha3


; Libraries ===================================================================

libraries[leaflet][download][type] = "file"
libraries[leaflet][download][url] = "https://github.com/CloudMade/Leaflet/zipball/v0.4.4"
libraries[leaflet][directory_name] = "leaflet"
