<?php
/**
 * @file
 * leafkick_demo.features.content.inc
 */

/**
 * Implements hook_content_defaults().
 */
function leafkick_demo_content_defaults() {
  $content = array();

  $content['golden_barley_hotel'] = (object) array(
    'title' => 'Golden Barley Hotel',
    'status' => '1',
    'promote' => '1',
    'sticky' => '0',
    'type' => 'venue',
    'language' => 'und',
    'created' => '1346481070',
    'comment' => '0',
    'translate' => '0',
    'machine_name' => 'golden_barley_hotel',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => 'Great Laksa and family friendly.',
          'summary' => '',
          'format' => 'plain_text',
          'safe_value' => '<p>Great Laksa and family friendly.</p>
',
          'safe_summary' => '',
        ),
      ),
    ),
    'field_leafkick_demo_address' => array(
      'und' => array(
        0 => array(
          'country' => 'AU',
          'administrative_area' => 'NSW',
          'sub_administrative_area' => NULL,
          'locality' => 'Marrickville',
          'dependent_locality' => NULL,
          'postal_code' => '2204',
          'thoroughfare' => '165-169 Edgeware Road',
          'premise' => '',
          'sub_premise' => NULL,
          'organisation_name' => NULL,
          'name_line' => NULL,
          'first_name' => NULL,
          'last_name' => NULL,
          'data' => NULL,
        ),
      ),
    ),
    'field_leafkick_demo_location' => array(
      'und' => array(
        0 => array(
          'wkt' => 'POINT (151.1745713 -33.9079708)',
          'geo_type' => 'point',
          'lat' => '-33.907971',
          'lon' => '151.174571',
          'left' => '151.174571',
          'top' => '-33.907971',
          'right' => '151.174571',
          'bottom' => '-33.907971',
          'srid' => NULL,
          'accuracy' => NULL,
          'source' => NULL,
        ),
      ),
    ),
  );

  $content['the_warren_view_hotel'] = (object) array(
    'title' => 'The Warren View Hotel',
    'status' => '1',
    'promote' => '1',
    'sticky' => '0',
    'type' => 'venue',
    'language' => 'und',
    'created' => '1346480697',
    'comment' => '0',
    'translate' => '0',
    'machine_name' => 'the_warren_view_hotel',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => 'Great beer garden.',
          'summary' => '',
          'format' => 'plain_text',
          'safe_value' => '<p>Great beer garden.</p>
',
          'safe_summary' => '',
        ),
      ),
    ),
    'field_leafkick_demo_address' => array(
      'und' => array(
        0 => array(
          'country' => 'AU',
          'administrative_area' => 'NSW',
          'sub_administrative_area' => NULL,
          'locality' => 'Enmore',
          'dependent_locality' => NULL,
          'postal_code' => '2042',
          'thoroughfare' => '2 Stanmore Road',
          'premise' => '',
          'sub_premise' => NULL,
          'organisation_name' => NULL,
          'name_line' => NULL,
          'first_name' => NULL,
          'last_name' => NULL,
          'data' => NULL,
        ),
      ),
    ),
    'field_leafkick_demo_location' => array(
      'und' => array(
        0 => array(
          'wkt' => 'POINT (151.170648 -33.899663)',
          'geo_type' => 'point',
          'lat' => '-33.899663',
          'lon' => '151.170648',
          'left' => '151.170648',
          'top' => '-33.899663',
          'right' => '151.170648',
          'bottom' => '-33.899663',
          'srid' => NULL,
          'accuracy' => NULL,
          'source' => NULL,
        ),
      ),
    ),
  );

  $content['town_hall_hotel'] = (object) array(
    'title' => 'Town Hall Hotel',
    'status' => '1',
    'promote' => '1',
    'sticky' => '0',
    'type' => 'venue',
    'language' => 'und',
    'created' => '1346480971',
    'comment' => '0',
    'translate' => '0',
    'machine_name' => 'town_hall_hotel',
    'body' => array(
      'und' => array(
        0 => array(
          'value' => 'Is it after 2 AM in the morning? Time to go to the Townie.',
          'summary' => '',
          'format' => 'plain_text',
          'safe_value' => '<p>Is it after 2 AM in the morning? Time to go to the Townie.</p>
',
          'safe_summary' => '',
        ),
      ),
    ),
    'field_leafkick_demo_address' => array(
      'und' => array(
        0 => array(
          'country' => 'AU',
          'administrative_area' => 'NSW',
          'sub_administrative_area' => NULL,
          'locality' => 'Newtown',
          'dependent_locality' => NULL,
          'postal_code' => '2042',
          'thoroughfare' => '326 King Street',
          'premise' => '',
          'sub_premise' => NULL,
          'organisation_name' => NULL,
          'name_line' => NULL,
          'first_name' => NULL,
          'last_name' => NULL,
          'data' => NULL,
        ),
      ),
    ),
    'field_leafkick_demo_location' => array(
      'und' => array(
        0 => array(
          'wkt' => 'POINT (151.178741 -33.898026)',
          'geo_type' => 'point',
          'lat' => '-33.898026',
          'lon' => '151.178741',
          'left' => '151.178741',
          'top' => '-33.898026',
          'right' => '151.178741',
          'bottom' => '-33.898026',
          'srid' => NULL,
          'accuracy' => NULL,
          'source' => NULL,
        ),
      ),
    ),
  );

return $content;
}
